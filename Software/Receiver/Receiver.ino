/* This is our basic receiver code, used for debugging. We send everything to serial Monitor and a LCD-Display*/


// *********************************************** INCLUDES ***********************************************
#include <RFM69.h>
#include <SPI.h>
#include <m3RFM.h>
#include <OneWire.h>
#include <LiquidCrystal.h>
#define NODEID        42   //unique for each node on same network
#define GATEWAYID     2



#define DS18S20_ID 0x10
#define DS18B20_ID 0x28



//#define LED_RED       5
#define LED_GREEN     8
#define BLINK_TIME    5

#define SERIAL_BAUD   19200


float temp;
int TRANSMITPERIOD = 20000; //transmit a packet to gateway so often (in ms)

byte payload[CUSTOMLENGTH] ;// not necessary, only for custom data which we don not really need here

long lastPeriod = -1;

byte retrycount = 1;

OneWire ds(5);
  LiquidCrystal lcd(6, 7, A2, A3, A4, A5);


// *******************************************************SETUP *************************************
void setup() {
  for(int i = 0; i < CUSTOMLENGTH; i++){
    payload[i] = 0 ;
  }
  
  Serial.begin(SERIAL_BAUD);
  
  
  Serial.println("***\n\nTransmitting at 433 Mhz...\n");
  pinMode(A2, OUTPUT);
  pinMode(A3, OUTPUT);
  pinMode(A4, OUTPUT);
  pinMode(A5, OUTPUT);

  
  

  
  lcd.begin(16, 2);
  lcd.print("hello, world!");
  lcd.display();
  

  if(radio.initialize(FREQUENCY, NODEID, NETWORKID)){Serial.println("INIT OK!");}
  radio.encrypt(ENCRYPTKEY);
  

  
// *************************************************************************************************

}


void serialOutputRFMData(){
  Serial.print('[');Serial.print(senderID, DEC);Serial.print("] ");
      for (byte i = 0; i < dataleng; i++)
        Serial.print((char)lastRec[i]);
      Serial.print("   [RX_RSSI:");Serial.print(rx_rssi);Serial.print("]");

   
   Serial.println(); 
}

void serialOutputACK(){
  Serial.println("ACK sent");
}


void processSerial(){
  //process any serial input
  if (Serial.available() > 0)
  {
    char input = Serial.read();
    if (input >= 48 && input <= 57) //[0,9]
    {
      TRANSMITPERIOD = 100 * (input-48);
      if (TRANSMITPERIOD == 0) TRANSMITPERIOD = 1000;
      
      // ... still too fast:
      TRANSMITPERIOD = TRANSMITPERIOD * 2;
      
      Serial.print("\nChanging delay to ");
      Serial.print(TRANSMITPERIOD);
      Serial.println("ms\n");
    }
    
    if (input == 'r')  // r=dump register values
      radio.readAllRegs();
    if (input == 'E')  // E=enable encryption
      radio.encrypt(ENCRYPTKEY);
    if (input == 'e')  // e=disable encryption
      radio.encrypt(null);
    // if (input == 'A')  // A=enable ACK
    //   requestACK = true;
    // if (input == 'a')  // a=disable ACK
    //   requestACK = false;
  }
}



  
    
    
void flash_led(byte color, int time)
{
  pinMode(color, OUTPUT);

  digitalWrite(color, HIGH);
  delay(time);
  digitalWrite(color, LOW);
}



void loop() {
 if(receiveRFM()){
   if(getKlasse() == SOUND || getKlasse() == ACK_SOUND){
     Serial.print("[");
     Serial.print(senderID, DEC);
     Serial.print("]-Mic:  ");
     Serial.println(getWert(), DEC);
//     lcd.setCursor(0,1);
//     lcd.print("Microfon: ");
//     lcd.print(getWert(), DEC);
//
   }
   if(getKlasse() == TEMP || getKlasse() == ACK_TEMP){
     Serial.print("[");
     Serial.print(senderID, DEC);
     Serial.print("]-Temp:  ");
     Serial.println(getFloatWert(), DEC);
     Serial.println(millis());
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("[");
     lcd.print(senderID);
     lcd.print("] ");
     lcd.print("Temp: ");
     lcd.print(getFloatWert(), 4);
   }
   if(getKlasse() == MOTION || getKlasse() == ACK_MOTION){
     Serial.print("[");
     Serial.print(senderID, DEC);
     Serial.print("]-Mot:  ");
     Serial.println(getWert(), DEC);
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("[");
     lcd.print(senderID);
     lcd.print("] ");
     lcd.print("Motion: ");
     lcd.print(getWert(), DEC);
   }
   if(getKlasse() == BATTERY || getKlasse() == ACK_BATTERY){
     Serial.print("[");
     Serial.print(senderID, DEC);
     Serial.print("]-Battery:  ");
     Serial.println(getWert(), DEC);
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("[");
     lcd.print(senderID);
     lcd.print("] ");
     lcd.print("Bat: ");
     lcd.print(getWert(), DEC);
     lcd.print("%");
   }
   if(getKlasse() == ROTARY || getKlasse() == ACK_ROTARY){
     Serial.print("[");
     Serial.print(senderID, DEC);
     Serial.print("]-ROTARY:  ");
     Serial.println(getWert(), DEC);
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("[");
     lcd.print(senderID);
     lcd.print("] ");
     lcd.print("Rotary: ");
     lcd.print(getWert(), DEC);
   }
   if(getKlasse() == OTHER || getKlasse() == ACK_OTHER){
     Serial.print("[");
     Serial.print(senderID, DEC);
     Serial.print("]-OTHER:  ");
     Serial.println(getWert(), DEC);
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print("[");
     lcd.print(senderID);
     lcd.print("] ");
     lcd.print("Other: ");
     lcd.print(getWert(), DEC);
   }
   if(ackSent()){
     serialOutputACK();
   } 
 }

}
