// Standard code for a sending sensor. the defines and globals determine what the sensor system does


#include <RFM69.h>
#include <SPI.h>
#include <m3RFM.h>
#include <OneWire.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>

#define NODEID          102   //unique for each node on same network
#define POS             1     //If Positions want to be used


#define DS18S20_ID      0x10
#define DS18B20_ID      0x28

  
#define LED_RED         8
#define BLINK_TIME      5
#define SERIAL_BAUD     19200

#define SUSPEND         0 //suspend for 8Sec after each looop?

#define BAT             1 // Check battery
#define BATLOOPS        2000 // number of loops before sending again
#define BATOFFSET       1666 //offset, so maybe you don't want each sensor to send at same time (espacially when microfon attached)
#define BATGATEWAY      42 // Gateway for Bat

#define TEMP            1 // temp-sensor attached?
#define TEMPLOOPS       500
#define TEMPOFFSET      450
#define TEMPGATEWAY     42 // Gateway

#define PIR             1 //PIR ?
#define PIRLOOPS        500
#define PIROFFSET       250
#define PIRGATEWAY      42 // Gateway PIR 

#define MIC             1 //Microfon ?
#define MICLOOPS        1
#define MICOFFSET       0
#define MICGATEWAY      42 // Gateway 


const int pinMic = A1;
const int pinTemp = 3;
const int pinPir = 4;
const int pinBat = A0;
float temp;
float bat;
int sumMic = 0;


OneWire ds(pinTemp);
long Loop = 0;



// *******************************************************SETUP *************************************
void setup() {
    
  Serial.begin(SERIAL_BAUD);
  
  Serial.println("***\n\nTransmitting at 433 Mhz...\n");
  
  if(PIR == 1){
    pinMode(pinPir, INPUT);
    digitalWrite(pinPir, LOW);
    delay(30000);
  }
  
  radio.initialize(FREQUENCY, NODEID, NETWORKID);
  radio.encrypt(ENCRYPTKEY);

  analogReference(DEFAULT);

  if(SUSPEND == 1){  
    /* Clear the reset flag. */
    MCUSR &= ~(1<<WDRF);
    
    /* In order to change WDE or the prescaler, we need to
     * set WDCE (This will allow updates for 4 clock cycles).
     */
    WDTCSR |= (1<<WDCE) | (1<<WDE);
  
    /* set new watchdog timeout prescaler value */
    WDTCSR = 1<<WDP0 | 1<<WDP3; /* 8.0 seconds */
    
    /* Enable the WD interrupt (note no reset). */
    WDTCSR |= _BV(WDIE);  
  }
}
  
// *************************************************************************************************


boolean getTemperature(){
 byte i;
 byte present = 0;
 byte data[12];
 byte addr[8];
 //find a device
 if (!ds.search(addr)) {
 ds.reset_search();
 return false;
 }
 if (OneWire::crc8( addr, 7) != addr[7]) {
 return false;
 }
 if (addr[0] != DS18S20_ID && addr[0] != DS18B20_ID) {
 return false;
 }
 ds.reset();
 ds.select(addr);
 // Start conversion
 ds.write(0x44, 1);
 // Wait some time...
 delay(850);
 present = ds.reset();
 ds.select(addr);
 // Issue Read scratchpad command
 ds.write(0xBE);
 // Receive 9 bytes
 for ( i = 0; i < 9; i++) {
 data[i] = ds.read();
 }
 // Calculate temperature value
 temp = ( (data[1] << 8) + data[0] )*0.0625;
 return true;
}
  
void flash_led(byte color, int time)
{
  pinMode(color, OUTPUT);

  digitalWrite(color, HIGH);
  delay(time);
  digitalWrite(color, LOW);
}


void getMic(){

  for(int i = 0; i < 10; i++){
    int valMic = analogRead(pinMic);
    sumMic = sumMic + valMic;
  }
  sumMic = sumMic / 10;
  Serial.println(sumMic);
}

boolean getMotion(){
  if(digitalRead(pinPir) == 1) return true;
  else return false;  
}

void getBat(){
 bat = analogRead(pinBat);
 bat = (bat / 1023) * 100;
 
}

void loop() {
  
  if((MIC == 1) && (Loop % MICLOOPS == MICOFFSET)){
    // Send Mic DATA
    getMic();
    setNextSend(SOUND, NODEID, POS, long(sumMic));
    sumMic = 0;
    sendRFM( MICGATEWAY );
    delay(20);
  }
  

  if((TEMP == 1) && (Loop % TEMPLOOPS == TEMPOFFSET)) {
    getTemperature();
    delay(100);
    setNextSend(ACK_TEMP, NODEID, POS, temp);
    if(sendRFM( TEMPGATEWAY)){
    delay(100);    
    }
  }
  
  if((PIR == 1) && (Loop % PIRLOOPS == PIROFFSET)){
    
    if(getMotion()) setNextSend(ACK_MOTION, NODEID, POS, long(1));
    else          setNextSend(ACK_MOTION, NODEID, POS, long(0));
    if(sendRFM( PIRGATEWAY )){
    delay(100);
    }  
    
  }
  
  
  if((BAT == 1) && (Loop % BATLOOPS == BATOFFSET)){
    
    getBat();
    setNextSend(ACK_BATTERY, 0x0066, 0x0F, long(bat));
    if(sendRFM( 255 )){
    delay(100);
    }  

  }  
  
  

  Loop ++; 
  
  if(Loop == 2147483647) Loop = 0;
  
  
  if( SUSPEND == 1){
    enterSleep();
  }
  
}


/***************************************************
 *  Name:        enterSleep
 *
 *  Returns:     Nothing.
 *
 *  Parameters:  None.
 *
 *  Description: Enters the arduino into sleep mode.
 *
 ***************************************************/
void enterSleep(void)
{
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);   /* EDIT: could also use SLEEP_MODE_PWR_DOWN for lowest power consumption. */
  sleep_enable();
  
  /* Now enter sleep mode. */
  sleep_mode();
  
  /* The program will continue from here after the WDT timeout*/
  sleep_disable(); /* First thing to do is disable sleep. */
  
  /* Re-enable the peripherals. */
  power_all_enable();
}







