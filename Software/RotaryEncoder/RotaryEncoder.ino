// The ROtaryEncoder parts of the code is based on:
/* RGB RingCoder Example Code
by: Jim Lindblom
SparkFun Electronics (date: February 14, 2012)
*/
//The comments for documentation of theese parts are used too


#include <RFM69.h>
#include <SPI.h>
#include <m3RFM.h>

#define NODEID        202   //unique for each node on same network
#define GATEWAYID     201 // 





// These three defines (ROTATION_SPEED, ENCODER_POSITION_MAX, and
// ENCODER_POSITION_MIN) control how fast the circular bar graph
// will fill up as you rotate the encoder.These depend on the
// encoderPosition variable being an unsigned 8-bit type.
#define ROTATION_SPEED 3 // MIN: 0, MAX: 5, 3 is a good value
#define ENCODER_POSITION_MAX (256 >> (ROTATION_SPEED - 1)) - 1
#define ENCODER_POSITION_MIN 0 // Don't go below 0
#include <PinChangeInt.h>

// Pin definitions - Encoder:
int aPin = 4; // Encoder A pin, used via software interrupt
int bPin = 7; // Encoder B pin, used via software interrupt
int redPin = 5; // Encoder's red LED - D5 is PWM enabled
int bluPin = 6; // Encoder's blue LED- D6 is PWM enabled
int grnPin = 3; // Encoder's green LED - D3 is PWM enabled
int swhPin = A0; // Encoder's switch pin

// Pin definitions - Shift registers:
int enPin = A5; // Shift registers' Output Enable pin
int latchPin = A4; // Shift registers' rclk pin
int clkPin = A3; // Shift registers' srclk pin
int clrPin = A2; // shift registers' srclr pin
int datPin = A1;// shift registers' SER pin

boolean changed = false;
boolean first = true;
long time = 0;

// The encoderPosition variable stores the position of the encoder.
// It's either incremented or decremented in the encoder's
// interrupt handler (readEncoder()). It's widely used in both the
// loop() and ledRingFiller() and ledRingFollower() functions to
// control the LEDs.
signed int encoderPosition; // Store the encoder's rotation counts
signed int oldPos;





void setup()
{
  Serial.begin (19200);
  Serial.println("Start");
  
  radio.initialize(FREQUENCY, NODEID, NETWORKID);
  radio.encrypt(ENCRYPTKEY);
  Serial.println("***\n\nTransmitting at 433 Mhz...\n");
  
  // Setup encoder pins, they should both be set as inputs
  // and internally pulled-up
  pinMode(aPin, INPUT);
  digitalWrite(aPin, HIGH);
  pinMode(bPin, INPUT);
  digitalWrite(bPin, HIGH);
  

  // just to be safe, let's not interrupt until everything's setup
  noInterrupts();
  // Attach interrupts to encoder pins. Whenever one of the encoder
  // pins changes (rise or fall), we'll go to readEncoder()
  PCintPort::attachInterrupt(aPin, readEncoder,CHANGE);
  PCintPort::attachInterrupt(bPin, readEncoder,CHANGE);
  
  // setup switch pins, set as an input, no pulled up
  pinMode(swhPin, INPUT);
  digitalWrite(swhPin, LOW); // Disable internal pull-up
  
  // Setup led pins as outputs, and write their intial value.
  // initial value is defined by the ledValue global variable
  pinMode(redPin, OUTPUT);
  analogWrite(redPin, 0); // Red on
  pinMode(grnPin, OUTPUT);
  analogWrite(grnPin, 255); // Green off
  pinMode(bluPin, OUTPUT);
  analogWrite(bluPin, 255); // Blue off
  
  // Setup shift register pins
  pinMode(enPin, OUTPUT); // Enable, active low, this'll always be LOW
  digitalWrite(enPin, LOW); // Turn all outputs on
  pinMode(latchPin, OUTPUT); // this must be set before calling shiftOut16()
  digitalWrite(latchPin, LOW); // start latch low
  pinMode(clkPin, OUTPUT); // we'll control this in shiftOut16()
  digitalWrite(clkPin, LOW); // start sck low
  pinMode(clrPin, OUTPUT); // master clear, this'll always be HIGH
  digitalWrite(clrPin, HIGH); // disable master clear
  pinMode(datPin, OUTPUT); // we'll control this in shiftOut16()
  digitalWrite(datPin, LOW); // start ser low
  
  // To begin, we'll turn all LEDs on the circular bar-graph OFF
  digitalWrite(latchPin, LOW); // first send latch low
  shiftOut16(0x0000);
  digitalWrite(latchPin, HIGH); // send latch high to indicate data is done sending
    
  // Now we can enable interrupts and start the code.
  interrupts();
}

void loop()
{
  // pressed? send it
  if (digitalRead(swhPin) == HIGH)
  {  
    noInterrupts();
    changed = false;
    first = true;
    
    int wert = encoderPosition;
    analogWrite(grnPin, 255);
    analogWrite(redPin, 0);
    
    
    setNextSend(ACK_ROTARY, 0x00CA, 0x01, long(wert));
    sendRFM(GATEWAYID);
    sendRFM(42);
    
    // do nothing while waiting for the switch to go back to LOW
    while(digitalRead(swhPin) == HIGH)
      ; // do nothing
      
    interrupts();  
  }
  
  //store rotation
  ledRingFiller(ROTATION_SPEED); // Update the Bar graph LED
  Serial.println(encoderPosition, DEC);
 
  // too long, jump back
  if(changed){
    first = false;
    if(millis() - time > 5000){
      noInterrupts();
      changed = false;
      encoderPosition = oldPos;
      first = true;
      analogWrite(redPin, 0);
      analogWrite(grnPin, 255);
      interrupts();
    }
  }
  
}


































void ledRingFiller(byte rotationSpeed)
{
  // ledShift stores the bit position of the upper-most LED
  // this value should be between 0 and 15 (shifting a 16-bit vaule)
  unsigned int ledShift = 0;
  unsigned int ledOutput = 0;
  // each bit of ledOutput represents a single LED on the ring
  // this should be a value between 0 and 0xFFFF (16 bits for 16 LEDs)
  
  
  // Only do this if encoderPosition = 0, if it is 0, we don't
  // want any LEDs lit up
  if (encoderPosition != 0)
  {
    // First set ledShift equal to encoderPosition, but we need
    // to compensate for rotationSpeed.
    ledShift = encoderPosition & (0xFF >> (rotationSpeed-1));
    // Now divide ledShift by 16, also compensate for rotationSpeed
    ledShift /= 0x10>>(rotationSpeed-1);
    // This for loop sets each bet that is less signfigant than
    // ledShift. This is what sets ledBarFiller apart from
    // ledBarFollower()
    for (int i=ledShift; i>=0; i--)
      ledOutput |= 1<<i;
  }
  
  // Now we just need to write to the shift registers. We have to
  // control latch manually, but shiftOut16 will take care of
  // everything else.
  digitalWrite(latchPin, LOW); // first send latch low
  shiftOut16(ledOutput); // send the ledOutput value to shiftOut16
  digitalWrite(latchPin, HIGH); // send latch high to indicate data is done sending
}



// This function'll call shiftOut (a pre-defined Arduino function)
// twice to shift 16 bits out. Latch is not controlled here, so you
// must do it before this function is called.
// data is sent 8 bits at a time, MSB first.
void shiftOut16(uint16_t data)
{
  byte datamsb;
  byte datalsb;
  
  // Isolate the MSB and LSB
  datamsb = (data&0xFF00)>>8; // mask out the MSB and shift it right 8 bits
  datalsb = data & 0xFF; // Mask out the LSB
  
  // First shift out the MSB, MSB first.
  shiftOut(datPin, clkPin, MSBFIRST, datamsb);
  // Then shift out the LSB
  shiftOut(datPin, clkPin, MSBFIRST, datalsb);
}

// readEncoder() is our interrupt handler for the rotary encoder.
// This function is called every time either of the two encoder
// pins (A and B) either rise or fall.
// This code will determine the directino of rotation, and
// update the global encoderPostion variable accordingly.
// This code is adapted from Rotary Encoder code by Oleg.
void readEncoder()
{
  noInterrupts(); // don't want our interrupt to be interrupted
  // First, we'll do some software debouncing. Optimally there'd
  // be some form of hardware debounce (RC filter). If there is
  // feel free to get rid of the delay. If your encoder is acting
  // 'wacky' try increasing or decreasing the value of this delay.
 // delayMicroseconds(5000); // 'debounce'
  if(first){
    oldPos = encoderPosition;
  }
  changed = true;
  time = millis();
  analogWrite(grnPin, 0);
  analogWrite(redPin, 255);
  // enc_states[] is a fancy way to keep track of which direction
  // the encoder is turning. 2-bits of oldEncoderState are paired
  // with 2-bits of newEncoderState to create 16 possible values.
  // Each of the 16 values will produce either a CW turn (1),
  // CCW turn (-1) or no movement (0).
  int8_t enc_states[] = {0,-1,1,0,1,0,0,-1,-1,0,0,1,0,1,-1,0};
  static uint8_t oldEncoderState = 0;
  static uint8_t newEncoderState = 0;

  // First, find the newEncoderState. This'll be a 2-bit value
  // the msb is the state of the B pin. The lsb is the state
  // of the A pin on the encoder.
  newEncoderState = (digitalRead(bPin)<<1) | (digitalRead(aPin));
  
  // Now we pair oldEncoderState with new encoder state
  // First we need to shift oldEncoder state left two bits.
  // This'll put the last state in bits 2 and 3.
  oldEncoderState <<= 2;
  // Mask out everything in oldEncoderState except for the previous state
  oldEncoderState &= 0x0C;
  // Now add the newEncoderState. oldEncoderState will now be of
  // the form: 0b0000(old B)(old A)(new B)(new A)
  oldEncoderState |= newEncoderState; // add filteredport value

  // Now we can update encoderPosition with the updated position
  // movement. We'll either add 1, -1 or 0 here.
  encoderPosition += enc_states[oldEncoderState];
  
  // This next bit will only happen if CONTINUOUS is not defined.
  // If CONTINUOUS is defined, encoderPosition will roll over from
  // -32768 (assuming it's a signed int) to to 32767 if decremented,
  // or 32767 to -32768 if incremented.
  // That can be useful for some applications. In this code, we
  // want the encoder value to stop at 255 and 0 (makes analog writing easier)
#ifndef CONTINUOUS
    // If encoderPosition is greater than the MAX, just set it
    // equal to the MAX
    if (encoderPosition > ENCODER_POSITION_MAX)
      encoderPosition = ENCODER_POSITION_MAX;
    // otherwise, if encoderPosition is less than the MIN, set it
    // equal to the MIN.
    else if (encoderPosition < ENCODER_POSITION_MIN)
      encoderPosition = ENCODER_POSITION_MIN;
#endif
  
  interrupts(); // re-enable interrupts before we leave
}
